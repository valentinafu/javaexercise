/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise;

public class ConsecutivePrimes {
      public static void main(String[] args) {
        int MAXIMUM = 1000000;
        int primeSum = 0;
        int currentNumber = 6;
        /*gjersa te jemi me pak se limiti (1milion) kontrollojme shumen e numrave prim me numrin aktual
        kontrollojme per numrin nqs eshte prim nepermjet metodes isPrime() ecila kthen nje vlere booleane true ose false
        e shtojme numrin ne shume dhe rrisim numrin aktual,afishojme ne fund shumen prim */
        while ((primeSum + currentNumber) < MAXIMUM) {
            if (isPrime(currentNumber)) {
                primeSum += currentNumber;
            }
            currentNumber++;
        }
        System.out.println(primeSum);
    }
    
    public static boolean isPrime(int n){
        boolean found=false;
        for(int i = 2; i <n/2; i++){
            if (n % i ==0) {
               found=true;
               break;
            }
        }
       return found;
    }
}
