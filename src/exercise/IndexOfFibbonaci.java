/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author Valentina
 */
public class IndexOfFibbonaci {
    public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    System.out.println("Enter number of digit");
    int numberofdigits=in.nextInt();
    //BigInteger format per te ruajtur numra shume te medhenj
    BigInteger f1=BigInteger.ONE;
    BigInteger f2= BigInteger.ONE;
    BigInteger f= BigInteger.ZERO;
    boolean done=true;
    int index=2;
    /*gjersa numri i shifrave qe ka futur perdoruesi te jete i njejte me ate te serisese fibbonacit
    kontrollojme ,ne momentin qe plotesohet kushti perfundojme ciklin, ruajme indeksin e cdo numri 
    te krijuar ne variablin index dhe afishojme*/
    while(done){
        f=f1.add(f2);
        index++;   
        f1=f2;
        f2=f;
        if(f.toString().length()==numberofdigits){
            done=false;
            break;
        }
    
    }
    System.out.println("Index is "+ index);
}
}
